﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraController : MonoBehaviour
{
    Vector3 touchStart;
    public float zoomOutMin = 1;
    public float zoomOutMax = 8;

    public Text endText;
    private int stickmansFound;

    // Start is called before the first frame update
    void Start()
    {
        stickmansFound = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            touchStart = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        }
        if (Input.GetMouseButton(0))
        {
            Vector3 direction = touchStart - Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Camera.main.transform.position += direction;
        }

        if (Input.touchCount == 2)
        {
            Touch touchOne = Input.GetTouch(0);
            Touch touchTwo = Input.GetTouch(1);
            Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;
            Vector2 touchTwoPrevPos = touchTwo.position - touchTwo.deltaPosition;
            float previousMagnitude = (touchOnePrevPos - touchTwoPrevPos).magnitude;
            float currentMagnitude = (touchOne.position - touchTwo.position).magnitude;
            float difference = currentMagnitude - previousMagnitude;
            zoom(difference * 0.01f);
        }

        if (Input.touchCount == 1)
        {
            if (Input.GetTouch(0).phase == TouchPhase.Began)
            {
                RaycastHit2D hitInfo = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position), Vector2.zero);

                //Debug.Log(hitInfo.collider);

                // RaycastHit2D can be either true or null, but has an implicit conversion to bool, so we can use it like this
                if (hitInfo)
                {
                    if (hitInfo.transform.gameObject.GetComponent<SpriteRenderer>().enabled.Equals(false))
                    {
                        stickmansFound += 1;
                    }

                    hitInfo.transform.gameObject.GetComponent<SpriteRenderer>().enabled = true;
                    // Here you can check hitInfo to see which collider has been hit, and act appropriately.
                }
            }
        }

        Boundary();


        if (stickmansFound.Equals(9))
        {
            endText.text = "ZNALEZIONO WSZYSTKICH";
        }
    }

    private void Boundary()
    {
        transform.position = new Vector3(
                                   Mathf.Clamp(transform.position.x, -6f, 6f),
                                   Mathf.Clamp(transform.position.y, -6f, 6f),
                                   Mathf.Clamp(transform.position.z, -10, -10));
    }

    private void zoom(float increment)
    {
        Camera.main.orthographicSize = Mathf.Clamp(Camera.main.orthographicSize - increment, zoomOutMin, zoomOutMax);
    }
}
